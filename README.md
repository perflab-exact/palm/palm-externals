<!-- -*-Mode: markdown;-*- -->
<!-- $Id$ -->

Palm Externals
=============================================================================

**Home**:
  - https://gitlab.com/perflab-exact/palm/palm-externals
  
  - [Performance Lab for EXtreme Computing and daTa](https://github.com/perflab-exact)


Preparing large files
=============================================================================

*Note*: For migration to GitLab/GitHub, the sample input/output files over 100 MB have been split. 

To recover them:
```sh
  for fnm_first in $(find . -name "*.split-aa") ; do
    fnm_base=${fnm_first%.split-aa}
    echo "*** Building ${fnm_base}"
    cat ${fnm_base}.split-* > ${fnm_base}
  done
  ```

To clean them:
  ```sh
  find . \( -name "*.bin" -o -name "*.m" \) -size +100M  -exec rm '{}' \;
  ```

To generate them:
  ```sh
  find . -name "boost_1_72_0*" -size +100M  -exec split -b 99MiB  '{}' '{}'.split- \;
  ```


Building & Installing
=============================================================================

<!-- git clone --depth=1 <repo> -->

  ```sh
  export PALM_EXT_ROOT=<palm-externals>

  export PALM_EXT_HPCTKEXT_ROOT=<hpctoolkit-externals-install>
  # example: ${PALM_EXT_ROOT}/hpctoolkit-externals/install
  
  cd ${PALM_EXT_ROOT}/hpctoolkit-externals
  mkdir MYBUILD && cd MYBUILD
  ${PALM_EXT_ROOT}/hpctoolkit-externals/configure \
    --prefix=${PALM_EXT_HPCTKEXT_ROOT}
  make install
  ```


PIN 3.x (for MIAMI-NW)
=============================================================================

  ```sh
  cd ${PALM_EXT_ROOT}/pin
  tar xzvf pin-3.7-97619-g0d0c92f4f-gcc-linux.tar.gz
  ```

IACA (for Palm/MemGaze task)
=============================================================================

  [[todo]]

-----------------------------------------------------------------------------
Perf (for Palm/MemGaze task)
=============================================================================

  [[todo]]


Notes:
=============================================================================

HPCToolkit Externals
-----------------------------------------------------------------------------

- Based on:

  HPCToolkit-externals, 2017.06 (2017/06/15 17:08:07)
  032dab135b5212fceafc958dc5cd12ce13a0c5d9
  
   ```sh
   export HPCTOOLKIT_EXT_REV=032dab135b5212fceafc958dc5cd12ce13a0c5d9
   git clone https://github.com/HPCToolkit/hpctoolkit-externals.git
   cd hpctoolkit-externals && git checkout ${HPCTOOLKIT_EXT_REV}
   ```

- DynInst (a.k.a. SymTabAPI)
  - Build all DynInst components
  - Disable "jump-table-slices" patch as it causes a crash

- Add 'ruby' package and 'distfiles'

- Flag changes to files with ".ORIG" suffix

- Note: Elfutils "lite" and DynInst "lite" packages contain full
  source tree but exclude documentation.


Deprecated: DynInst (Using HPCToolkit Externals) (now built above)
-----------------------------------------------------------------------------

Currently using latest DynInst (9.3.2).
  
  http://www.dyninst.org
  https://github.com/dyninst/dyninst/releases

1. Build

   ```sh
   export HPCTOOLKIT_EXT=${HOME}/hpctoolkit/hpctoolkit-externals/x86_64-linux
   cd <dyninst>
   mkdir MYBUILD && cd MYBUILD
   cmake -LH ..
   cmake \
     -DCMAKE_INSTALL_PREFIX=`pwd`/../MYINSTALL \
     -DCMAKE_BUILD_TYPE=Release \
     -DCROSS_COMPILING=1 \
     -DCMAKE_C_COMPILER=/opt/rh/devtoolset-2/root/usr/bin/gcc \
     -DCMAKE_CXX_COMPILER=/opt/rh/devtoolset-2/root/usr/bin/g++ \
     -DBoost_INCLUDE_DIR=${HPCTOOLKIT_EXT}/boost/include \
     -DBoost_LIBRARY_DIR=${HPCTOOLKIT_EXT}/boost/lib \
     -DLIBELF_INCLUDE_DIR=${HPCTOOLKIT_EXT}/libelf/include \
     -DLIBELF_LIBRARIES=${HPCTOOLKIT_EXT}/libelf/lib/libelf.so \
     -DLIBDWARF_INCLUDE_DIR=${HPCTOOLKIT_EXT}/libdwarf/include \
     -DLIBDWARF_LIBRARIES=${HPCTOOLKIT_EXT}/libdwarf/lib/libdwarf.so \
     -DIBERTY_LIBRARIES=${HPCTOOLKIT_EXT}/binutils/lib/libiberty.a \
     ..
	 
   make -j8
   make install
  ```

Dependencies:
----------------------------------------
 DynInstAPI > DynInstAPI_RT > PatchAPI > ParseAPI > SymtabAPI

DynInst Execution options:
----------------------------------------
  DYNINST_DEBUG_STARTUP=1 
  DYNINST_DEBUG_PARSE=1
  DYNINST_DEBUG_PATCHAPI=1
  DYNINST_DEBUG_INST=1
  DYNINST_DEBUG_BPATCH=1
  DYNINST_DEBUG_PARSING=1
  DYNINST_DEBUG_RELOCATION=1
  


Boost (Using HPCToolkit Externals)
-----------------------------------------------------------------------------

If new Boost is needed:
  ./bootstrap.sh --prefix=${HOME}/pkg/boost-1-58-0
  ./b2 install


`
Xed (Using HPCToolkit Externals)
-----------------------------------------------------------------------------

https://intelxed.github.io
https://github.com/intelxed/xed

